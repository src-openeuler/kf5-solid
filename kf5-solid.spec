%global framework solid

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 1 integration module that provides hardware information

License:        LGPLv2+
URL:            https://solid.kde.org/

%global majmin %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0: https://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

## upstreamable patches

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros >= %{majmin}
BuildRequires:  libupnp-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  systemd-devel

%if ! 0%{?bootstrap}
# Predicate parser deps
BuildRequires:  bison
BuildRequires:  flex
# really runtime-only dep, but doesn't hurt to check availability at buildtime
BuildRequires:  media-player-info
BuildRequires:  pkgconfig(libimobiledevice-1.0)
BuildRequires:  pkgconfig(mount)
Requires:       media-player-info
Requires:       udisks2
Requires:       upower
%endif

Requires:       kf5-filesystem

Obsoletes:      kf5-solid-libs < 5.47.0-2
Provides:       kf5-solid-libs = %{version}-%{release}
Provides:       kf5-solid-libs = %{version}-%{release}

%description
Solid provides the following features for application developers:
 - Hardware Discovery
 - Power Management
 - Network Management

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
%{cmake_kf5} -DWITH_NEW_POWER_ASYNC_API:BOOL=ON  -DWITH_NEW_POWER_ASYNC_FREEDESKTOP:BOOL=ON  -DWITH_NEW_SOLID_JOB:BOOL=ON
%{cmake_build}

%install
%{cmake_install}

%find_lang_kf5 solid5_qt


%files -f solid5_qt.lang
%doc README.md TODO
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_bindir}/solid-hardware5
%{_kf5_bindir}/solid-power
%{_kf5_qmldir}/org/kde/solid/
%{_kf5_libdir}/libKF5Solid.so.*

%files devel
%{_kf5_includedir}/Solid/
%{_kf5_libdir}/libKF5Solid.so
%{_kf5_libdir}/cmake/KF5Solid/
%{_kf5_archdatadir}/mkspecs/modules/qt_Solid.pri


%changelog
* Wed Jan 15 2025 misaka00251 <liuxin@iscas.ac.cn> - 5.116.0-1
- Update package to version 5.116.0

* Thu Nov 07 2024 Funda Wang <fundawang@yeah.net> - 5.115.0-2
- adopt to new cmake macro

* Fri Mar 01 2024 maqi <maqi@uniontech.com> - 5.115.0-1
- Update package to version 5.115.0

* Fri Jan 05 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 5.113.0-1
- Update package to version 5.113.0

* Thu Aug 03 2023 zhangkea <zhangkea@uniontech.cn> - 5.108.0-1
- Update version to 5.108.0

* Mon Apr 24 2023 peijiankang <peijiankang@kylinos.cn> - 5.100.0-2
- rebuild

* Mon Dec 12 2022 liqiuyu <liqiuyu@kylinos.cn> - 5.100.0-1
- Update package to version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 peijiankang <peijiankang@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Fri Jan 14 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Thu Jul 23 2020 Zhao Yang <yangzhao1@kylinos.cn> - 5.55.0-1
- Initial release for OpenEuler
